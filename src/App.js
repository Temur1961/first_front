import './styles/bootstrap/bootstrap.scss'
import './styles/theme/App.sass';
import Header from './App/views/Header/'
import Footer from './App/views/Footer/'

function App() {
    return (
        <div className="App">
            <Header/>
            <Footer/>
        </div>
    );
}

export default App;

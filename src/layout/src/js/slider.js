
import Swiper, {
  Autoplay,
  Pagination
} from 'swiper';

const slider = new Swiper(".feedback-wrapper", {
  modules: [Pagination, Autoplay],
  slidesPerView: 2,
  spaceBetween: 30,
  loop: true,
  autoplay: {
    delay: 5000,
    disableOnInteraction: false,
  },
  pagination: {
    el: '.swiper-pagination-bull',
    type: "bullets",
    bulletClass: "bull",
    bulletActiveClass: "bull-active",
    clickable: true
  },
});

